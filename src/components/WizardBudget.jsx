import Box from "@mui/material/Box";
import React, { useContext, useState } from "react";
import HomeTitle from "./HomeTitle";
import Logo from "./logo/Logo";
import WizardText from "./WizardText";
import { Grid, useMediaQuery } from "@mui/material";
import AmountInputWizard from "./AmountInputWizard";
import ButtonComponent from "./ButtonComponent";
import { useNavigate } from "react-router-dom";
import { Context } from "../context/Context";

export default function WizardBudget() {
  const navigate = useNavigate();
  const [userAmountInput, setUserAmountInput] = useState("");
  const { getAmountFromUSer } = useContext(Context);
  const matches = useMediaQuery("(min-width:600px)");

  return (
    <Grid container>
      <Logo />
      <Grid item xs={12} md={6}>
        <Box paddingTop={9}>
          <HomeTitle title={"WELCOME"} />
        </Box>
        <Box marginTop={7}>
          <WizardText wizardText={"How much money you have at the moment?"} />
        </Box>

        <form>
          <AmountInputWizard
            value={userAmountInput}
            onChange={(e) => {
              setUserAmountInput(e.target.value);
            }}
          />
          <ButtonComponent
            text={"ADD"}
            width={"100%"}
            marginTop={35}
            color={"white"}
            backgroundColor={"#652fe2"}
            isDisabled={userAmountInput > 0 ? false : true}
            handleSubmit={() => {
              getAmountFromUSer(userAmountInput);
              navigate("/SelectCategories/");
            }}
          />
        </form>
      </Grid>
    </Grid>
  );
}
