import { Box, Grid } from "@mui/material";
import React from "react";
import ExpencePerCategory from "./ExpencePerCategory";
import HomeTitle from "./HomeTitle";
import Logo from "./logo/Logo";
import WizardText from "./WizardText";

export default function ExpencePerCategoryComponent() {
  return (
    <Grid container>
      <Logo />

      <Grid item xs={12} md={6}>
        <Box paddingTop={9}>
          <HomeTitle title={"WELCOME"} />
        </Box>
        <Box marginTop={7}>
          <WizardText
            wizardText={
              "Set how much money you want to spend on each Category monthly"
            }
          />
        </Box>
        <Box>
          <ExpencePerCategory />
        </Box>
      </Grid>
    </Grid>
  );
}
