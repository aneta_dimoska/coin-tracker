import React, { createContext, useEffect, useState } from "react";

export const Context = createContext({});

export const Provider = ({ children }) => {
  const [amount, setAmount] = useState("");
  const [dataAPI, setDataAPI] = useState([]);
  // const [checkedCategoriesArr, setcheckedCategoriesArr] = useState();
  const [amountPerCategory, setAmountPerCategory] = useState();
  // current category selected
  const [currentCategoryId, setCurrentCategoryId] = useState(null);
  // stores total income amount from entries
  const [entryTotal, setEntryTotal] = useState(0);

  const [categories, setCategories] = useState([
    {
      id: 1,
      type: "expense",
      name: "Food",
      budget: undefined,
      icon: "dinner_dining",
      isEnabled: true,
      entries: [],
    },
    {
      id: 2,
      type: "expense",
      name: "Travel",
      budget: undefined,
      icon: "travel_explore",
      isEnabled: true,
      entries: [],
    },
    {
      id: 3,
      type: "expense",
      name: "Shopping",
      budget: undefined,
      icon: "shopping_bag",
      isEnabled: false,
      entries: [],
    },
    {
      id: 4,
      type: "expense",
      name: "Utilities",
      budget: undefined,
      icon: "receipt",
      isEnabled: false,
      entries: [],
    },
    {
      id: 5,
      type: "expense",
      name: "Rent",
      budget: undefined,
      icon: "house",
      isEnabled: false,
      entries: [],
    },
    {
      id: 6,
      type: "expense",
      name: "Savings",
      budget: undefined,
      icon: "savings",
      isEnabled: false,
      entries: [],
    },
    {
      id: 7,
      type: "expense",
      name: "Gym",
      budget: undefined,
      icon: "fitness_center",
      isEnabled: false,
      entries: [],
    },
  ]);

  // fetcging data
  useEffect(() => {
    fetch("https://randomuser.me/api/")
      .then((res) => res.json())
      .then((data) => setDataAPI(data.results[0]));
  }, []);

  // updating LS on every change of categories
  useEffect(() => {
    localStorage.setItem("categories", JSON.stringify(categories));
  }, [categories]);

  // get the amount per category in first step of welcome wizard
  const getAmountFromUSer = (_amount) => {
    setAmount(_amount);
  };

  // get the amount per category in 3rd step of welcome wizard
  const getAmountPerCategoryFromUser = (_categoryAmount) => {
    setAmountPerCategory(_categoryAmount);
  };

  // Add Category
  const addCategory = (category) => {
    setCategories([{ ...category, id: new Date().valueOf() }, ...categories]);
  };

  // Update Category
  const updateCategory = (category) => {
    const updated = categories.map((eachCategory) => {
      if (eachCategory.id === category.id) {
        return category;
      } else {
        return eachCategory;
      }
    });

    setCategories(updated);
  };

  // Delete Category
  const deleteCategory = (category) => {
    const updated = categories.filter(
      (eachCategory) => eachCategory.id !== category.id
    );

    setCategories(updated);
  };

  // Add Entry
  const addEntry = (entry) => {
    categories.map((eachCategory) => {
      if (eachCategory.id === currentCategoryId) {
        // adding new entry to the entries array in the current category
        eachCategory.entries.push({
          ...entry,
          id: Math.floor(Math.random() * 10001),
        });
        // calculating the total of income amount per entries in the current category
        eachCategory.entries.map((eachEntry) =>
          setEntryTotal(eachEntry.amount + entryTotal)
        );
      }
    });

    localStorage.setItem("categories", JSON.stringify(categories));
  };

  // Update Entry
  const updateEntry = (entry) => {
    categories.map((eachCategory) => {
      if (eachCategory.id === currentCategoryId) {
        return entry;
      } else {
        return eachCategory;
      }
    });
  };
  // Delete entry
  const deleteEntry = (entry) => {
    const updated = categories.filter(
      (eachCategory) => eachCategory.id !== entry.id
    );

    setCategories(updated);
  };
  const contextObj = {
    categories,
    dataAPI,
    amount,
    amountPerCategory,
    currentCategoryId,
    entryTotal,
    addEntry,
    setCategories,
    getAmountFromUSer,
    getAmountPerCategoryFromUser,
    addCategory,
    updateCategory,
    deleteCategory,
    updateEntry,

    setCurrentCategoryId,
  };

  return <Context.Provider value={contextObj}>{children}</Context.Provider>;
};
