import React, { useContext, useState } from "react";
import Button from "@mui/material/Button";
import { Context } from "../context/Context";

export default function ButtonComponent(props) {
  return (
    <Button
      variant="contained"
      className={"buttonStyles"}
      sx={[
        { width: props.width },
        { marginTop: props.marginTop },
        { color: props.color },
        { backgroundColor: props.backgroundColor },
      ]}
      onClick={props.handleSubmit}
      disabled={props.isDisabled}
    >
      {props.text}
    </Button>
  );
}
