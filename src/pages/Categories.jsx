import React, { useContext, useEffect, useState } from "react";
import MenuBottom from "../components/MenuBottom";
import MenuComponent from "../components/MenuComponent";
import {
  Box,
  Dialog,
  Fab,
  Icon,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Paper,
  Typography,
} from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import styled from "@emotion/styled";
import { Context } from "../context/Context";
import AddEditCategory from "../components/AddEditCategory";

// styled components
const StyledFab = styled(Fab)`
  position: absolute;
  bottom: 40px;
  right: 10px;
  background-color: #09f7e7;
  color: black;
`;
const StyledBox = styled(Box)`
  background-color: ${(props) => props.open === false && "yellow"};
`;
const StyledModal = styled(Dialog)`
  .MuiBackdrop-root.css-yiavyu-MuiBackdrop-root-MuiDialog-backdrop {
    background-color: rgba(255, 255, 255, 0.5);
  }
`;

export default function Categories() {
  const { categories, setCategories, dataAPI } = useContext(Context);
  // state for AddEditCategory modal
  const [openModal, setOpenModal] = useState(false);
  // storing clicked category
  const [selectedCategory, setSelectedCategory] = useState();

  // if data in LS set it in categories, otherwise take the initial categories
  useEffect(() => {
    if (localStorage.getItem("categories")) {
      setCategories(JSON.parse(localStorage.getItem("categories")));
    } else {
      setCategories(categories);
    }
  }, []);

  // close the AddEdit Category modal
  const handleClose = () => {
    setOpenModal(false);
  };

  return (
    <StyledBox open={openModal}>
      <MenuComponent title={"Categories"} dataAPI={dataAPI} />
      <Paper elevation={5} sx={{ marginTop: "25px" }}>
        <Typography
          variant="body1"
          component="h5"
          sx={{
            textAlign: "left",
            backgroundColor: "#EBEBEB",
            color: "gray",
            padding: "6px",
          }}
        >
          Categories
        </Typography>
        <List>
          <ListItem disablePadding>
            <ListItemButton
              onClick={() => {
                setOpenModal(true);
                setSelectedCategory(null);
              }}
            >
              <ListItemIcon>
                <AddIcon />
              </ListItemIcon>
              <ListItemText primary={"Add New Category"} />
            </ListItemButton>
          </ListItem>
          {categories.map(
            (eachCategory) =>
              eachCategory.isEnabled && (
                <ListItem
                  key={eachCategory.id}
                  disablePadding
                  onClick={() => {
                    setOpenModal(true);
                    setSelectedCategory(eachCategory); // stores each of the categories that is clicked
                  }}
                >
                  <ListItemButton
                    sx={
                      eachCategory.type === "expense"
                        ? { color: "red" }
                        : { color: "green" }
                    }
                    divider
                  >
                    <ListItemIcon>
                      <Icon
                        sx={
                          eachCategory.type === "expense"
                            ? { color: "red" }
                            : { color: "green" }
                        }
                      >
                        {eachCategory.icon}
                      </Icon>
                    </ListItemIcon>
                    <ListItemText primary={eachCategory.name} />
                    {eachCategory.budget ? (
                      <ListItemText
                        primary={eachCategory.budget}
                        sx={{ textAlign: "right" }}
                      />
                    ) : (
                      <ListItemText
                        primary={"No budget Limit"}
                        classes={{ textAlign: "right", fontSize: "8px" }}
                      />
                    )}
                  </ListItemButton>
                </ListItem>
              )
          )}
        </List>
      </Paper>
      <StyledModal open={openModal} onClose={handleClose}>
        <AddEditCategory
          category={selectedCategory} // takes clicked category
          onClose={handleClose}
          // onSuccess={handleSuccess}
        />
      </StyledModal>
      <MenuBottom />
      <StyledFab color="primary" aria-label="add">
        <AddIcon />
      </StyledFab>
    </StyledBox>
  );
}
