import { Typography } from "@mui/material";
import React from "react";

export default function WizardText(props) {
  return (
    <Typography sx={[{ fontSize: 17 }]} component="p">
      {props.wizardText}
    </Typography>
  );
}
