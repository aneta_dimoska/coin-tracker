import React, { useContext, useState } from "react";
import Box from "@mui/material/Box";
import useSetUpPage from "../hooks/useSetUpPage";
import ButtonComponent from "./ButtonComponent";
import { useNavigate } from "react-router-dom";
import HomeTitle from "./HomeTitle";
import UsernameInput from "./UsernameInput";
import PasswordInput from "./PasswordInput";
import LoginText from "./LoginText";
import Footer from "./Footer";
import useBooleanValue from "../hooks/useBooleanValue";
import useValidationInputFeedback from "../hooks/useValidationInputFeedback";
import Logo from "./logo/Logo";
import { Grid } from "@mui/material";
import { Context } from "../context/Context";

export default function LoginAccount(props) {
  const navigate = useNavigate();
  const { dataAPI } = useContext(Context);
  const [input, setInput, handleResetInput] = useSetUpPage();
  const [password, setPassword, handleResetPassword] = useSetUpPage();

  // check if input is empty
  const [isErrorUsername, handleErrorUsername] = useBooleanValue(false);
  const [isErrorPassword, handleErrorPassword] = useBooleanValue(false);

  // display message if input is empty
  const [validationFeedbackUsername, setValidationFeedbackUsername] =
    useValidationInputFeedback();
  const [validationFeedbackPassword, setValidationFeedbackPassword] =
    useValidationInputFeedback();

  // password shown
  const [isShownPassword, setisShownPassword] = useState(false);

  const handleClickShowPassword = () => {
    setisShownPassword(!isShownPassword);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (input !== "" && password !== "") {
      handleResetInput();
      handleResetPassword();
      navigate("/Overview/");
      localStorage.setItem("usernameLogin", input);
      localStorage.setItem("dataAPI", JSON.stringify(dataAPI));
      return;
    }

    if (input === "" && password === "") {
      handleErrorUsername();
      setValidationFeedbackUsername(`Please enter a username`);
      handleErrorPassword();
      setValidationFeedbackPassword(`Please enter a password`);
      return;
    }

    if (input === "" || password !== "") {
      handleErrorUsername();
      setValidationFeedbackUsername(`Please enter a username`);
      return;
    }
    if (password === "" || input !== "") {
      handleErrorPassword();
      setValidationFeedbackPassword(`Please enter a password`);
      return;
    }
  };

  return (
    <Grid container paddingTop={5}>
      <Logo />
      <Grid item xs={12} md={6}>
        <Box paddingTop={9}>
          <HomeTitle title={"SIGN IN"} />
        </Box>
        <form onSubmit={handleSubmit}>
          <UsernameInput
            error={isErrorUsername}
            placeholder="someone@example.com"
            value={input}
            onChange={setInput}
            helperText={isErrorUsername && validationFeedbackUsername}
          />

          <PasswordInput
            error={isErrorPassword}
            isShownPassword={isShownPassword}
            value={password}
            onChange={setPassword}
            onClick={handleClickShowPassword}
            validationFeedbackPassword={validationFeedbackPassword}
          />

          <ButtonComponent
            text={"SIGN IN"}
            width={"40%"}
            marginTop={9}
            color={"white"}
            backgroundColor={"#652fe2"}
            // route={"/Overview/"}
            handleSubmit={handleSubmit}
          />
        </form>
        <LoginText
          content={"Don't have account yet?"}
          path={"/SignUpPage/"}
          linkTitle={"Sign up now, it is free!"}
        />

        <Footer />
      </Grid>
    </Grid>
  );
}
