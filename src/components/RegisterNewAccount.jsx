import React, { useState } from "react";
import validator from "validator";
import { useNavigate } from "react-router-dom";
import Box from "@mui/material/Box";
import ButtonComponent from "./ButtonComponent";
import Footer from "./Footer";
import HomeTitle from "./HomeTitle";
import LoginText from "./LoginText";
import PasswordInput from "./PasswordInput";
import UsernameInput from "./UsernameInput";
import useSetUpPage from "../hooks/useSetUpPage";
import useBooleanValue from "../hooks/useBooleanValue";
import useValidationInputFeedback from "../hooks/useValidationInputFeedback";
import { Grid } from "@mui/material";
import Logo from "./logo/Logo";

export default function RegisterNewAccount() {
  const navigate = useNavigate();
  const [usernameEmail, handleUsernameValue, handleRestInput] =
    useSetUpPage("");
  const [password, handlePassword, handleResetPassword] = useSetUpPage("");

  // check if input is empty
  const [isErrorUsername, handleErrorUsername] = useBooleanValue(false);
  const [isErrorPassword, handleErrorPassword] = useBooleanValue(false);

  // display message if input is empty
  const [validationFeedbackUsername, setValidationFeedbackUsername] =
    useValidationInputFeedback();
  const [validationFeedbackPassword, setValidationFeedbackPassword] =
    useValidationInputFeedback();

  // password shown
  const [isShownPassword, setisShownPassword] = useState(false);

  const handleClickShowPassword = () => {
    setisShownPassword(!isShownPassword);
  };

  const validPassword = new RegExp(
    /(?=^.{8,32}$)(?=(?:.*?\d){1})(?=.*[a-z])(?=(?:.*?[!@#$%^&*}{:;?.]){1})(?!.*\s)[0-9a-zA-Z!@#$%^&*]*$/
  );

  const handleSubmitNewAccount = (e) => {
    e.preventDefault();
    if (usernameEmail !== "" && password !== "") {
      handleRestInput();
      handleResetPassword();
      navigate("/WizardAmountPage/");
      localStorage.setItem("usernameSignUp", usernameEmail);
    }

    if (usernameEmail === "" && password === "") {
      handleErrorUsername();
      setValidationFeedbackUsername(`Please enter a username`);
      handleErrorPassword();
      setValidationFeedbackPassword(`Please enter a password`);
      return;
    }

    if (usernameEmail === "" || password !== "") {
      handleErrorUsername();
      setValidationFeedbackUsername(`Please enter a username`);
      return;
    }
    if (password === "" || usernameEmail !== "") {
      handleErrorPassword();
      setValidationFeedbackPassword(`Please enter a password`);
      return;
    }
  };

  return (
    <Grid container paddingTop={5}>
      <Logo />
      <Grid item xs={12} md={6}>
        <Box paddingTop={9}>
          <HomeTitle title={"SIGN UP"} />
        </Box>
        <form onSubmit={handleSubmitNewAccount}>
          <UsernameInput
            placeholder="someone@example.com"
            error={isErrorUsername}
            value={usernameEmail}
            onChange={handleUsernameValue}
            helperText={isErrorUsername && validationFeedbackUsername}
          />

          <PasswordInput
            error={isErrorPassword}
            isShownPassword={isShownPassword}
            value={password}
            onChange={handlePassword}
            onClick={handleClickShowPassword}
            validationFeedbackPassword={validationFeedbackPassword}
          />

          <ButtonComponent
            text={"SIGN UP"}
            width={"40%"}
            marginTop={9}
            color={"white"}
            backgroundColor={"#652fe2"}
            handleSubmit={handleSubmitNewAccount}
            isDisabled={
              validator.isEmail(usernameEmail) && validPassword.test(password)
                ? false
                : true
            }
          />
        </form>
        <LoginText
          content={"Already have account?"}
          path={"/"}
          linkTitle={"Sign in please"}
        />

        <Footer />
      </Grid>
    </Grid>
  );
}
