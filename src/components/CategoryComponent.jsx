import React, { useContext, useEffect, useState } from "react";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemText from "@mui/material/ListItemText";
import Checkbox from "@mui/material/Checkbox";
import { Context } from "../context/Context";
import ButtonComponent from "./ButtonComponent";
import { useNavigate } from "react-router-dom";
import ListItemIcon from "@mui/material/ListItemIcon";
import Icon from "@mui/material/Icon";

export default function CheckboxListSecondary() {
  const navigate = useNavigate();
  // const [checked, setChecked] = useState([]);
  const [isDisabled, setIsDisabled] = useState(true);
  const { categories, setCategories } = useContext(Context);

  useEffect(() => {
    if (localStorage.getItem("categories")) {
      setCategories(JSON.parse(localStorage.getItem("categories")));
    } else {
      setCategories(categories);
    }
  }, [isDisabled]);

  const handleToggle = (value) => () => {
    let newCategories = categories.slice();
    setIsDisabled(true);
    newCategories.map((category) => {
      if (category.id === value.id) {
        category.isEnabled = !category.isEnabled;
      }

      if (category.isEnabled) {
        setIsDisabled(false);
      }
    });

    setCategories(newCategories);
  };

  return (
    <>
      <List
        dense
        sx={{
          width: "100%",
          maxWidth: 360,
          bgcolor: "background.paper",
          marginTop: "30px",
        }}
      >
        {categories.map((category) => {
          return (
            <ListItem
              divider
              key={category.id}
              secondaryAction={
                <Checkbox
                  edge="end"
                  onChange={handleToggle(category)}
                  checked={category.isEnabled}
                />
              }
              disablePadding
            >
              <ListItemIcon>
                <Icon>{category.icon}</Icon>
              </ListItemIcon>

              <ListItemButton>
                <ListItemText id={category.id} primary={category.name} />
              </ListItemButton>
            </ListItem>
          );
        })}
      </List>

      <ButtonComponent
        text={"DONE"}
        width={"100%"}
        marginTop={35}
        color={"white"}
        backgroundColor={"#652fe2"}
        isDisabled={isDisabled}
        handleSubmit={() => {
          localStorage.setItem("categories", JSON.stringify(categories));
          navigate("/CategoryExpense/");
        }}
      />
    </>
  );
}
