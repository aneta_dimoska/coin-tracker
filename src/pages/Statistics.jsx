import React from "react";
import MenuBottom from "../components/MenuBottom";
import MenuComponent from "../components/MenuComponent";
import styled from "@emotion/styled";
import { Fab } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";

const StyledFab = styled(Fab)`
  position: absolute;
  bottom: 40px;
  right: 10px;
  background-color: #09f7e7;
  color: black;
`;

export default function Statistics() {
  const dataFromLS = JSON.parse(localStorage.getItem("dataAPI"));
  return (
    <>
      <MenuComponent title={"Statistics"} dataFromLS={dataFromLS} />

      <MenuBottom />
      <StyledFab color="primary" aria-label="add">
        <AddIcon />
      </StyledFab>
    </>
  );
}
