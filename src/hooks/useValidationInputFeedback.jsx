import React, { useEffect, useState } from "react";

export default function useValidationInputFeedback() {
  const [validationFeedbackInput, setValidationFeedbackInput] = useState();

  useEffect(() => {
    setValidationFeedbackInput(`${validationFeedbackInput}`);
  }, [validationFeedbackInput]);

  return [validationFeedbackInput, setValidationFeedbackInput];
}
