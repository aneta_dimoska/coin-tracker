import React from "react";
import Box from "@mui/material/Box";
import Logo from "./logo/Logo";
import HomeTitle from "./HomeTitle";
import WizardText from "./WizardText";
import CategoryComponent from "./CategoryComponent";
import { Grid } from "@mui/material";

export default function SelectInitalCategories() {
  return (
    <Grid container>
      <Logo />
      <Grid item xs={12} md={6}>
        <Box paddingTop={9}>
          <HomeTitle title={"WELCOME"} />
        </Box>
        <Box marginTop={7}>
          <WizardText wizardText={"Choose what you spend money on"} />
        </Box>
        <Box>
          <CategoryComponent />
        </Box>
      </Grid>
    </Grid>
  );
}
