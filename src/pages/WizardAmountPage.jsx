import React from "react";

import WizardBudget from "../components/WizardBudget";

export default function WizardAmountPage() {
  return (
    <Container>
      <Box>
        <WizardBudget />
      </Box>
    </Container>
  );
}
