import React from "react";

export default function Icon(props) {
  return React.createElement(props.icon, {});
}
