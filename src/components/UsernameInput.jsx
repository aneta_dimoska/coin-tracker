import React from "react";
import TextField from "@mui/material/TextField";

export default function UsernameInput(props) {
  return (
    <TextField
      // cursor to be pointed in the input automatically
      fullWidth
      placeholder={props.placeholder}
      error={props.error}
      id="outlined-basic"
      label="Username"
      variant="outlined"
      value={props.value}
      onChange={props.onChange}
      helperText={props.helperText}
      sx={[{ marginTop: 5 }, {}]}
    />
  );
}
