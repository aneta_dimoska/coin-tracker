import { FilledInput, FormControl, InputLabel } from "@mui/material";
import React from "react";

export default function AmountInputWizard(props) {
  return (
    <FormControl fullWidth sx={{ marginTop: 8 }} variant="filled">
      <InputLabel htmlFor="filled-adornment-amount">Amount</InputLabel>
      <FilledInput
        type="number"
        id="filled-adornment-amount"
        value={props.value}
        onChange={props.onChange}
        label="Amount"
      />
    </FormControl>
  );
}
