import React from "react";
import BottomNavigation from "@mui/material/BottomNavigation";
import BottomNavigationAction from "@mui/material/BottomNavigationAction";
import HomeIcon from "@mui/icons-material/Home";
import CategoryIcon from "@mui/icons-material/Category";
import LeaderboardIcon from "@mui/icons-material/Leaderboard";
import { Paper } from "@mui/material";
import { Link } from "react-router-dom";
import { styled } from "@mui/material/styles";

export default function MenuBottom() {
  // const [value, setValue] = React.useState(0);

  const CustomizedBottomNavigation = styled(BottomNavigation)`
    background-color: #652fe2;
    justify-content: start;

    & .MuiBottomNavigationAction-root {
      margin-left: 5px;
      padding-right: 30px;
    }

    & .MuiBottomNavigationAction.Mui-active {
      color: #652fe2;
    }

    & .MuiBottomNavigationAction-label {
      opacity: 1;
    }
  `;

  return (
    <Paper
      sx={{
        position: "fixed",
        bottom: 0,
        left: 0,
        right: 0,
        color: "red",
        component: "div",
        bgcolor: "#652fe2",
      }}
    >
      <CustomizedBottomNavigation
        // showLabels
        sx={{ width: "100%" }}
      >
        <Link to={"/Overview/"}>
          <BottomNavigationAction
            label="Overview"
            icon={<HomeIcon sx={{ color: "white" }} />}
          />
        </Link>

        <Link to={"/Categories/"}>
          <BottomNavigationAction
            label="Categories"
            icon={<CategoryIcon sx={{ color: "white" }} />}
          />
        </Link>

        <Link to={"/Statistics/"}>
          <BottomNavigationAction
            label="Statistics"
            icon={<LeaderboardIcon sx={{ color: "white" }} />}
          />
        </Link>
      </CustomizedBottomNavigation>
    </Paper>
  );
}
