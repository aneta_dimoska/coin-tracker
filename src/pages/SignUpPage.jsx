import React from "react";
import RegisterNewAccount from "../components/RegisterNewAccount";
import { Box, Container } from "@mui/material";

export default function SignUpPage() {
  return (
    <Container>
      <Box>
        <RegisterNewAccount />
      </Box>
    </Container>
  );
}
